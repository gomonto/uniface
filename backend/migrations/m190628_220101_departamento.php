<?php

use yii\db\Schema;

class m190628_220101_departamento extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('departamento', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            ], $tableOptions);
                
    }

    public function down()
    {
        $this->dropTable('departamento');
    }
}
