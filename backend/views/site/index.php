<?php
/* @var $this yii\web\View */

use backend\models\Departamento;
/*
 * lib dataprovider
 */
use yii\data\ActiveDataProvider;

use kartik\cmenu\ContextMenu;


$this->title = 'My Yii Application';
$this->params['breadcrumbs'][] = ['label' => 'site', 'url' => ['site/index']];
?>


<?php
/*
 * calculos de datos
 */
$rows = (new \yii\db\Query())
        ->select(['count(*) as total'
        ])
        ->from('departamento')
        ->one();
/*
 * 
 */
$departamento = Departamento::findOne(2);
/*
 * manejo de dataproviders
 */
$dpDepartamentos = new ActiveDataProvider([
    'query' => Departamento::find()
            ->orderBy('id'),
    'pagination' => false,
        ]);


$c = 1;
?>
    <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Departamento</span>
              <span class="info-box-number"><?php echo $departamento->nombre; ?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
              <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
            </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Bordered Table</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
            <tr>
                <th style="width: 10px">#</th>
                <th>Task</th>
                <th>Progress</th>
                <th style="width: 40px">Label</th>
            </tr>
            <?php 
            $c=1;
            foreach ($dpDepartamentos->models as $modelos) { ?>
            <tr>
                <td><?php echo $c?></td>
                <td><?php echo $modelos->nombre ?></td>
                <td>
                    <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" style="width: <?php echo $modelos->id* rand(1, 10) ?>%"></div>
                    </div>
                </td>
                <td><span class="badge bg-red"><?php echo $modelos->id*10 ?></span></td>
            </tr>
            <?php
            $c++; 
            }?>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
        </div>



        
 
// Basic context menu usage on a specific text within a paragaph.
<?php
$items = [
    ['label'=>'Action', 'url'=>'#'],
    ['label'=>'Another action', 'url'=>'#'],
    ['label'=>'Something else here', 'url'=>'#'],
    '<li class="divider"></li>',
    ['label'=>'Separated link', 'url'=>'#'],
];
ContextMenu::begin(['items'=>$items]);
echo '<span class="kv-context">Right click here.</span>';
ContextMenu::end();
 
// Context menu usage on a div container
ContextMenu::begin(['items'=>$items, 'options'=>['tag'=>'div']]);
echo '<div class="card p-3 bg-light">Right click anywhere inside this container.</div>';
ContextMenu::end();
 
// Activate context menu within a div but not for `spans` within the div
$script = <<< 'JS'
function (e, element, target) {
    e.preventDefault();
    if (e.target.tagName == 'SPAN') {
        e.preventDefault();
        this.closemenu();
        return false;
    }
    return true;
}
JS;
ContextMenu::begin([
    'items'=>$items,
    'options'=>['tag'=>'div'],
    'pluginOptions'=>['before'=>$script]
]);
echo '<div class="card p-3 bg-light">Right click anywhere inside this container but note that
    <span class="text-danger">context is disabled in this highlighted span element</span></div>';
ContextMenu::end();
?>